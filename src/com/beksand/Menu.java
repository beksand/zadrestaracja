package com.beksand;

import java.util.ArrayList;
import java.util.List;

public class Menu implements AllDania{
    private List<Dania> menu = new ArrayList<>();

    public Menu() {
    }

    public Menu(List<Dania> menu) {
        this.menu = menu;
    }

    @Override
    public void addSkladniki() {

    }

    @Override
    public void addDania() {
        Dania dania = new Dania();
        dania.addSkladniki();
        menu.add(dania);
    }
}
