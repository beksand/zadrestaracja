package com.beksand;

//---
//        Pewna znana restauracja chce wprowadzić nowy sposób wydawania posiłków. Podajemy jakie mamy alergie, a program
// wyświetla nam dania jakie możemy zamówić. W tym celu przygotuj klasę, która będzie reprezentować menu. Powinna zawierać
// nazwę posiłku, składniki i cenę. Każdy składnik będzie reprezentowany przez klasę, która ma mieć nazwę składnika,
// cenę oraz alergeny.
//
//        Przygotowujemy interfejs, który pozwoli dodać składniki oraz dania.
//
//        Następnie podajemy na jakie alergeny jesteśmy uczuleni, a program wyświetla nam jakie potrawy możemy zamówić
// oraz cenę tych dań.


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Menu restaracja = new Menu();
        Dania dania = null;
        List<Skladnik> skladniks = new ArrayList<>();
        dania = new Dania("Ratatuj", skladniks, 100.3);
        Skladnik skladnik = new Skladnik("Pomidor", "jakis_alergen", 10.5);
        restaracja.addDania();
        Scanner sc = new Scanner(System.in);
        System.out.println("Jaki masz aliergeny: ");
        String alergen = sc.nextLine();
        if (!alergen.equals(skladnik.getAlergeny())){
            System.out.println(dania);
        }
    }
}
