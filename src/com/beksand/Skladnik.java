package com.beksand;

public class Skladnik {
    private String nazwe;
    private String alergeny;
    private double cena;

    public Skladnik(String nazwe, String alergeny, double cena) {
        this.nazwe = nazwe;
        this.alergeny = alergeny;
        this.cena = cena;
    }

    public Skladnik() {
    }

    public String getNazwe() {
        return nazwe;
    }

    public void setNazwe(String nazwe) {
        this.nazwe = nazwe;
    }

    public String getAlergeny() {
        return alergeny;
    }

    public void setAlergeny(String alergeny) {
        this.alergeny = alergeny;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }
}
